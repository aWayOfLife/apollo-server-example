const { ApolloServer, PubSub } = require('apollo-server-express')
const express = require('express')
const { resolvers } = require('./schema/resolvers')
const {typeDefs} = require('./schema/typeDefs')
const http = require('http')
const cors = require('cors')




const app = express()

app.use(cors())




const server = new ApolloServer({
    typeDefs,
    resolvers,
    subscriptions: {
    path: '/subscriptions',
    onConnect: (connectionParams, webSocket, context) => {
      console.log('Client connected');
    },
    onDisconnect: (webSocket, context) => {
      console.log('Client disconnected')
    },
  }
})

server.applyMiddleware({app})

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen(5000, () => {
  console.log(`🚀 Server ready at http://localhost:5000/graphql`)
  console.log(`🚀 Subscriptions ready at ws://localhost:5000/graphql`)
})