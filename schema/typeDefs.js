const {gql} = require('apollo-server-express')

const typeDefs = gql `
    type User{
        name:String!
        age: Int
        married: Boolean
    }

    #Queries
    type Query{
        users:[User!]!
        user(name:String):User
    }

    #Mutations
    type Mutation{
        createUser(name:String, age:Int, married:Boolean):User
    }

    #Subscriptions
    type Subscription{
        newUser:User!
    }
  

`

module.exports = {typeDefs}