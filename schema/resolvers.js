const {users} = require('../fakeData')
const { ApolloServer, PubSub } = require('apollo-server-express')
const pubSub = new PubSub()
const resolvers = {
    Subscription:{
        newUser:{
            subscribe:()=>{
                return pubSub.asyncIterator('NEW USER')
            }
        }
    },
    Query:{
        users(){
            return users
        },
        user(parent, args){
            return users.find(user => user.name === args.name)

            
        }
    },
    Mutation:{
        createUser(parent,args){

            const newUser = args
            users.push(newUser)
            pubSub.publish('NEW USER',{
                newUser:newUser
            })
            return newUser
        }
    },

}

module.exports = {resolvers}