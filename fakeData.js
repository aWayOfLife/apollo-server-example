let users = [
    {
        name:"Harsh",
        age:19,
        married:false
    },
    {
        name:"Arshi",
        age:20,
        married:false
    },
    {
        name:"Pankaj",
        age:24 ,
        married:true
    },
    {
        name:"Disha",
        age:21,
        married:false
    },
    {
        name:"Abhirup",
        age:19,
        married:true
    },
    {
        name:"Sounak",
        age:19,
        married:false
    },
]

module.exports = {users}